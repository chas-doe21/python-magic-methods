"""A Namespace is a dictionary where we can access keys via `.`-syntax.
"""

class Namespace(dict):
    def __getattr__(self, name):
        return self[name]

    def __setattr__(self, key, value):
        self[key] = value


class MyDefaultDict(dict):
    def __init__(self, default_value):
        self.default_value = default_value

    def __getitem__(self, k):
        if not k in self.keys():
            self[k] = self.default_value()
        return super().__getitem__(k)