"""FuzzyFloat implements a float type that only allows integer values or values
   with the decimal part as .5
"""

from math import ceil, floor

class FuzzyFloat:
    def __init__(self, value=0):
            self.value = self.fuzzy_rounding(value)

    def __repr__(self):
        return f"FuzzyFloat({self.value})"

    def __str__(self):
        return f"{self.value}"

    def fuzzy_rounding(self, x):
        print(f"Fuzzy rounding {x} ", end="")
        if x % 1 > 0.5:
            x = ceil(x)
        elif x % 1 < 0.5:
            x = floor(x)
        print(f"to {x}")
        
        return x

    def __add__(self, x):
        result = self.value + x
        
        return FuzzyFloat(result)

    def __mul__(self, x):
        result = self.value * x
        
        return FuzzyFloat(result)

    def __truediv__(self, x):
        result = self.value / x
        
        return FuzzyFloat(result)

    def __sub__(self, x):
        result = self.value - x
        
        return FuzzyFloat(result)

    def __mod__(self, x):
        result = self.value % x
        
        return FuzzyFloat(result)

    def __eq__(self, other):
        return self.value == other.value
