# Homework

- Implement a list type that only allows values in the odd indexes (1,3,5, etc)
  and fills even-indexes with None

- Implement a numeric type that raises an error if the result of an operation is
  3, otherwise it behaves like an int

- Implement a dict type that automatically encodes its values into json